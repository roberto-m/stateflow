package com.robertomartucci.stateflow.detail

import android.content.Context
import android.view.Window
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatDialog
import androidx.core.content.ContextCompat
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.databinding.LayoutReviewDialogBinding
import com.robertomartucci.stateflow.models.Product

class ReviewDialog(context: Context, product: Product, private val listener: OnReviewDialogListener)
    : AppCompatDialog(context, true, null) {

    interface OnReviewDialogListener {

        fun onNewReview(text: String, rating: Int)
    }

    private var binding: LayoutReviewDialogBinding

    init {
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = LayoutReviewDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    setRating(progress)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

                override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit

            })

        setRating(0)

        val title = context.getString(R.string.review_product_title, product.name)
        binding.titleTv.text = title

        binding.cancelIv.setOnClickListener { dismiss() }

        binding.sendButton.apply {
            setBackgroundColor(ContextCompat.getColor(context, R.color.design_default_color_secondary))
            setOnClickListener {
                listener.onNewReview(binding.reviewEt.text.toString().trim(), binding.seekBar.progress)
                dismiss()
            }
        }
    }

    private fun setRating(progress: Int) {
        binding.ratingTv.text = "$progress"
    }

}