package com.robertomartucci.stateflow.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.robertomartucci.stateflow.models.Product
import com.robertomartucci.stateflow.models.Review
import com.robertomartucci.stateflow.repositories.resources.Resource
import com.robertomartucci.stateflow.repositories.reviews.IReviewsRepository
import com.robertomartucci.stateflow.util.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class DetailViewModel @Inject constructor(
        private val reviewsRepository: IReviewsRepository,
        private val dispatchers: DispatcherProvider
) : ViewModel() {

    lateinit var product: Product

    private val _detailEvent = MutableStateFlow<DetailEvent>(DetailEvent.Empty)
    val detailEvent: StateFlow<DetailEvent> = _detailEvent

    sealed class DetailEvent {
        data class Success(val review: Review?) : DetailEvent()
        data class Failure(val message: String?) : DetailEvent()
        object Loading : DetailEvent()
        object Empty : DetailEvent()
    }

    fun onNewReview(text: String, rating: Int) {
        _detailEvent.value = DetailEvent.Loading

        viewModelScope.launch(dispatchers.io) {
            when (val response = reviewsRepository.postReview(product.id, text, rating)) {
                is Resource.Error -> _detailEvent.value = DetailEvent.Failure(response.message)
                is Resource.Success -> {
                    with(response.data) {
                        _detailEvent.value = DetailEvent.Success(this)
                    }
                }
            }
        }
    }
}