package com.robertomartucci.stateflow.detail

import android.view.View
import com.robertomartucci.stateflow.databinding.LayoutReviewListItemBinding
import com.robertomartucci.stateflow.base.BaseVieHolder
import com.robertomartucci.stateflow.models.Review

class ReviewViewHolder(itemView: View) : BaseVieHolder<Review>(itemView) {

    override fun updateUI(item: Review?) {
        val binding = LayoutReviewListItemBinding.bind(itemView)
        binding.reviewTv.text = item?.text
        binding.ratingTv.text = "${item?.rating}"
    }
}