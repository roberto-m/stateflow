package com.robertomartucci.stateflow.detail

import android.view.View
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.base.BaseAdapter
import com.robertomartucci.stateflow.models.Review

class ReviewsAdapter(reviews: List<Review>) : BaseAdapter<Review, ReviewViewHolder>() {

    init {
        items = reviews.toMutableList()
    }

    override val layoutId: Int
        get() = R.layout.layout_review_list_item

    override fun onCreateHolder(view: View, viewType: Int, listener: OnItemClickListener<Review>?): ReviewViewHolder {
        return ReviewViewHolder(view)
    }
}