package com.robertomartucci.stateflow.detail

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.databinding.FragmentDetailBinding
import com.robertomartucci.stateflow.interfaces.IFabFragment
import com.robertomartucci.stateflow.main.MainViewModel
import com.robertomartucci.stateflow.models.Product
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class DetailFragment : Fragment(), IFabFragment, ReviewDialog.OnReviewDialogListener {

    private lateinit var adapter: ReviewsAdapter

    override val fabLabel: String?
        get() {
            return activity?.getString(R.string.review)
        }

    override val fabIcon: Drawable?
        get() {
            return activity?.let {
                ContextCompat.getDrawable(it, android.R.drawable.ic_input_add)
            }
        }

    private val detailViewModel: DetailViewModel by viewModels()

    private var binding: FragmentDetailBinding? = null

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val transition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        sharedElementEnterTransition = transition
        sharedElementReturnTransition = transition
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailBinding.bind(view)

        arguments?.getParcelable<Product>(getString(R.string.product))?.let {
            (activity as? AppCompatActivity)?.supportActionBar?.title = it.name

            detailViewModel.product = it

            setFlowState()

            binding?.apply {
                val price = "${it.price} ${it.currency}"
                priceTv.text = price
                descriptionTv.text = it.description

                imageIv.post {
                    context?.let { context ->
                        Glide.with(context)
                                .load(it.imgUrl)
                                .centerCrop()
                                .transform(RoundedCorners(context.resources.getDimensionPixelSize(R.dimen.product_image_corner_radius)))
                                .placeholder(R.drawable.ic_broken_image)
                                .into(imageIv)
                    }
                }
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.setHasFixedSize(true)
                adapter = ReviewsAdapter(it.reviews)
                recyclerView.adapter = adapter

                if(it.reviews.isEmpty()) {
                    recyclerView.visibility = View.INVISIBLE
                    infoTv.visibility = View.VISIBLE
                }

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                            val threshold = 10
                            if (dy >= -threshold && dy <= threshold) {
                                return
                            }
                            if (dy > threshold) {
                                mainViewModel.shrink()
                                return
                            }
                            mainViewModel.extend()
                        }
                    })
                }
            }
        }
    }

    private fun setFlowState() {
        lifecycleScope.launchWhenStarted {
            detailViewModel.detailEvent.collect {
                when(it) {
                    is DetailViewModel.DetailEvent.Failure -> {
                        hideLoading()
                        context?.let { ctx ->
                            showSnackBar(it.message ?: ctx.getString(R.string.something_went_wrong))
                        }
                    }
                    is DetailViewModel.DetailEvent.Loading -> {
                        showLoading()
                    }
                    is DetailViewModel.DetailEvent.Success -> {
                        hideLoading()
                        it.review?.let { review ->
                            adapter.items.add(0, review)
                            adapter.notifyDataSetChanged()

                            context?.let { ctx ->
                                showSnackBar(ctx.getString(R.string.thanks))
                            }

                            binding?.apply {
                                infoTv.visibility = View.GONE
                                recyclerView.visibility = View.VISIBLE
                            }
                        }
                    }
                    is DetailViewModel.DetailEvent.Empty -> {
                        hideLoading()
                    }
                }
            }
        }
    }

    private fun showSnackBar(message: String) {
        mainViewModel.showSnackBar(message)
    }

    private fun showLoading() {
        binding?.progressBar?.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding?.progressBar?.visibility = View.GONE
    }

    override fun onFabClick() {
        context?.let {
            ReviewDialog(it, detailViewModel.product, this).show()
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    override fun onNewReview(text: String, rating: Int) {
        detailViewModel.onNewReview(text, rating)
    }
}