package com.robertomartucci.stateflow.repositories.reviews

import android.content.Context
import com.robertomartucci.stateflow.models.Review
import com.robertomartucci.stateflow.repositories.BaseRepository
import com.robertomartucci.stateflow.repositories.resources.Resource
import com.robertomartucci.stateflow.repositories.resources.ResourceList
import retrofit2.awaitResponse
import java.util.*
import javax.inject.Inject

class ReviewsRepository @Inject constructor(
        private val api: IReviewsApi,
        context: Context
): BaseRepository<Review>(context), IReviewsRepository {

    override suspend fun getReviews(productId: String): ResourceList<Review> {
        return try {
            val response = api.getReviews(productId).awaitResponse()
            getResourceListResult(response)
        } catch (e: Exception) {
            getResourceListFailure(e)
        }
    }

    override suspend fun postReview(productId: String, text: String, rating: Int): Resource<Review> {

        val review = Review(locale = Locale.getDefault().toLanguageTag(),
            productId = productId,
            text = text,
            rating = rating)

        return try {
            val response = api.postReview(productId, review).awaitResponse()
            getResourceResult(response)
        } catch (e: Exception) {
            getResourceFailure(e)
        }
    }
}