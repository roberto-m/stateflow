package com.robertomartucci.stateflow.repositories.reviews

import com.robertomartucci.stateflow.models.Review
import com.robertomartucci.stateflow.repositories.resources.Resource
import com.robertomartucci.stateflow.repositories.resources.ResourceList

interface IReviewsRepository {

    suspend fun getReviews(productId: String): ResourceList<Review>

    suspend fun postReview(productId: String, text: String, rating: Int): Resource<Review>
}