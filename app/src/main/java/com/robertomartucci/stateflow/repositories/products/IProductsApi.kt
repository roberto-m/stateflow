package com.robertomartucci.stateflow.repositories.products

import com.robertomartucci.stateflow.models.Product
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface IProductsApi {

    @GET("/product")
    fun getProducts(): Call<List<Product>>

    @GET("/product/{id}")
    fun getProduct(@Path("id") productId: String): Call<Product>
}