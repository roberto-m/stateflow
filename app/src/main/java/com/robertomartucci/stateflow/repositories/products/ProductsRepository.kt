package com.robertomartucci.stateflow.repositories.products

import android.content.Context
import com.robertomartucci.stateflow.models.Product
import com.robertomartucci.stateflow.repositories.BaseRepository
import com.robertomartucci.stateflow.repositories.resources.Resource
import com.robertomartucci.stateflow.repositories.resources.ResourceList
import retrofit2.awaitResponse
import javax.inject.Inject

class ProductsRepository @Inject constructor(
        private val api: IProductsApi,
        context: Context
) : BaseRepository<Product>(context), IProductsRepository {

    override suspend fun getProducts(): ResourceList<Product> {
        return try {
            val response = api.getProducts().awaitResponse()
            getResourceListResult(response)
        } catch (e: Exception) {
            getResourceListFailure(e)
        }
    }

    override suspend fun getProduct(productId: String): Resource<Product> {
        return try {
            val response = api.getProduct(productId).awaitResponse()
            getResourceResult(response)
        } catch (e: Exception) {
            getResourceFailure(e)
        }
    }
}