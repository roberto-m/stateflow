package com.robertomartucci.stateflow.repositories.products

import com.robertomartucci.stateflow.models.Product
import com.robertomartucci.stateflow.repositories.resources.Resource
import com.robertomartucci.stateflow.repositories.resources.ResourceList

interface IProductsRepository {

    suspend fun getProducts(): ResourceList<Product>

    suspend fun getProduct(productId: String): Resource<Product>
}