package com.robertomartucci.stateflow.repositories

import android.content.Context
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.repositories.resources.Resource
import com.robertomartucci.stateflow.repositories.resources.ResourceList
import retrofit2.Response
import java.net.ConnectException

open class BaseRepository<T>(private val context: Context) {


    fun getResourceResult(response: Response<T>): Resource<T> {
        if(response.isSuccessful) {
            return Resource.Success(response.body())
        }
        return Resource.Error(response.message())
    }

    fun getResourceFailure(e: Exception): Resource<T> {
        return Resource.Error(failureMessage(e))
    }

    private fun failureMessage(e: Exception): String {
        val message = when(e) {
            is ConnectException -> context.getString(R.string.check_connection_error)
            else -> e.message
        }
        return context.getString(R.string.an_error_occurred, message ?: context.getString(R.string.unknown))
    }

    fun getResourceListResult(response: Response<List<T>>): ResourceList<T> {
        if(response.isSuccessful) {
            return ResourceList.Success(response.body() ?: listOf())
        }
        return ResourceList.Error(response.message())
    }

    fun getResourceListFailure(e: Exception): ResourceList<T> {
        return ResourceList.Error(failureMessage(e))
    }
}