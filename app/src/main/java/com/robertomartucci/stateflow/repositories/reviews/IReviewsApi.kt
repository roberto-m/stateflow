package com.robertomartucci.stateflow.repositories.reviews

import com.robertomartucci.stateflow.models.Review
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface IReviewsApi {

    @GET("/reviews/{id}")
    fun getReviews(@Path("id") productId: String): Call<List<Review>>

    @POST("/reviews/{id}")
    fun postReview(@Path("id") productId: String, @Body review: Review): Call<Review>
}