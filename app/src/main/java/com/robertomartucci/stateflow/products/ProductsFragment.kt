package com.robertomartucci.stateflow.products

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.robertomartucci.stateflow.interfaces.ISearchFragment
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.base.BaseAdapter
import com.robertomartucci.stateflow.databinding.FragmentProductsBinding
import com.robertomartucci.stateflow.databinding.LayoutProductListItemBinding
import com.robertomartucci.stateflow.models.Product
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ProductsFragment : Fragment(), ISearchFragment, BaseAdapter.OnItemClickListener<Product> {

    private var binding: FragmentProductsBinding? = null

    private val viewModel: ProductsViewModel by viewModels()

    private val adapter = ProductsAdapter(this@ProductsFragment)

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentProductsBinding.bind(view).apply {
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = adapter
            recyclerView.setHasFixedSize(true)

            swipeLayout.setOnRefreshListener {
                getProducts()
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.productsEvent.collect {
                when(it) {
                    is ProductsViewModel.ProductsEvent.Failure -> {
                        showInfoMessage(it.message ?: getString(R.string.something_went_wrong))
                    }
                    is ProductsViewModel.ProductsEvent.Loading -> {
                        showLoading()
                    }
                    is ProductsViewModel.ProductsEvent.Success -> {
                        adapter.apply {
                            items.clear()
                            items.addAll(it.products ?: mutableListOf())
                            notifyDataSetChanged()
                        }
                        hideLoading()
                    }
                    is ProductsViewModel.ProductsEvent.Empty -> {
                        showInfoMessage(getString(R.string.no_products_found))
                    }
                }
            }
        }

        getProducts()
    }

    private fun getProducts() {
        viewModel.getProducts()
    }

    private fun showInfoMessage(message: String) {
        hideLoading()
        binding?.apply {
            recyclerView.visibility = View.GONE
            infoTv.text = message
            infoTv.visibility = View.VISIBLE
        }
    }

    private fun showLoading() {
        binding?.apply {
            recyclerView.visibility = View.GONE
            infoTv.visibility = View.GONE
            if(!swipeLayout.isRefreshing) {
                progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun hideLoading() {
        binding?.apply {
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            infoTv.visibility = View.GONE
            swipeLayout.isRefreshing = false
        }
    }

    private fun showDetail(product: Product?, itemView: View) {
        val bundle = Bundle().apply {
            putParcelable(context?.getString(R.string.product), product)
        }
        val binding = LayoutProductListItemBinding.bind(itemView)

        val image = getString(R.string.image)
        val price = getString(R.string.price)
        val description = getString(R.string.description)

        ViewCompat.setTransitionName(binding.imageIv, image)
        ViewCompat.setTransitionName(binding.priceTv, price)
        ViewCompat.setTransitionName(binding.descriptionTv, description)

        val extras = FragmentNavigator.Extras.Builder().addSharedElements(
                mapOf(binding.imageIv to image,
                        binding.priceTv to price,
                        binding.descriptionTv to description)).build()
        findNavController().navigate(R.id.action_ProductsFragment_to_DetailFragment, bundle, null, extras)
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    override fun onQueryTextSubmit(query: String?) = Unit

    override fun onQueryTextChange(newText: String?) {
        viewModel.filterItems(newText)
    }

    override fun onItemClick(item: Product?, itemView: View) {
        showDetail(item, itemView)
    }
}