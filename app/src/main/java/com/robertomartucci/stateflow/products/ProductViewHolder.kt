package com.robertomartucci.stateflow.products

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.base.BaseVieHolder
import com.robertomartucci.stateflow.databinding.LayoutProductListItemBinding
import com.robertomartucci.stateflow.models.Product

class ProductViewHolder(itemView: View) : BaseVieHolder<Product>(itemView) {

    override fun updateUI(item: Product?) {
        val binding = LayoutProductListItemBinding.bind(itemView)
        binding.nameTv.text = item?.name
        binding.descriptionTv.text = item?.description

        with("${item?.price} ${item?.currency}") {
            binding.priceTv.text = this
        }

        Glide.with(context)
            .load(item?.imgUrl)
            .centerCrop()
            .transform(RoundedCorners(context.resources.getDimensionPixelSize(R.dimen.product_image_corner_radius)))
            .placeholder(R.drawable.ic_broken_image)
            .into(binding.imageIv)
    }
}