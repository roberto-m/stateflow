package com.robertomartucci.stateflow.products

import android.view.View
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.base.BaseAdapter
import com.robertomartucci.stateflow.models.Product

class ProductsAdapter(listener: OnItemClickListener<Product>?) :
    BaseAdapter<Product, ProductViewHolder>(listener) {

    override val layoutId: Int
        get() = R.layout.layout_product_list_item

    override fun onCreateHolder(
        view: View,
        viewType: Int,
        listener: OnItemClickListener<Product>?
    ): ProductViewHolder {
        return ProductViewHolder(view)
    }
}