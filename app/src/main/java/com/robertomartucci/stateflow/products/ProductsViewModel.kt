package com.robertomartucci.stateflow.products

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.robertomartucci.stateflow.models.Product
import com.robertomartucci.stateflow.repositories.resources.ResourceList
import com.robertomartucci.stateflow.repositories.products.IProductsRepository
import com.robertomartucci.stateflow.util.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class ProductsViewModel @Inject constructor(
        private val productsRepository: IProductsRepository,
        private val dispatchers: DispatcherProvider
) : ViewModel() {

    private var totalItems: List<Product>? = mutableListOf()

    private val _productsState = MutableStateFlow<ProductsEvent>(ProductsEvent.Loading)

    val productsEvent: StateFlow<ProductsEvent> = _productsState

    sealed class ProductsEvent {
        data class Success(val products: List<Product>?) : ProductsEvent()
        data class Failure(val message: String?) : ProductsEvent()
        object Loading : ProductsEvent()
        object Empty : ProductsEvent()
    }

    fun getProducts() = viewModelScope.launch(dispatchers.io) {
        _productsState.value = ProductsEvent.Loading
        when (val response = productsRepository.getProducts()) {
            is ResourceList.Error -> _productsState.value = ProductsEvent.Failure(response.message)
            is ResourceList.Success -> {
                with(response.data) {
                    totalItems = this
                    notifyNewItems(totalItems)
                }
            }
        }
    }

    private fun notifyNewItems(items: List<Product>?) {
        if(items?.isEmpty() == true) {
            _productsState.value = ProductsEvent.Empty
            return
        }
        _productsState.value = ProductsEvent.Success(items)
    }

    fun filterItems(newText: String?) {
        _productsState.value = ProductsEvent.Loading
        val items = newText?.let { totalItems?.filter { it.name.contains(newText) || it.description.contains(newText) } } ?: emptyList()
        notifyNewItems(items)
    }

}