package com.robertomartucci.stateflow.main

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.robertomartucci.stateflow.R
import com.robertomartucci.stateflow.databinding.ActivityMainBinding
import com.robertomartucci.stateflow.interfaces.ISearchFragment
import com.robertomartucci.stateflow.interfaces.IFabFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity(),
    FragmentManager.OnBackStackChangedListener, SearchView.OnQueryTextListener {

    private lateinit var navController: NavController

    private lateinit var binding: ActivityMainBinding

    private var toolbarSearchView: SearchView? = null

    private lateinit var navHostFragment: NavHostFragment

    private val viewModel: MainViewModel by viewModels()

    private val currentFragment: Fragment?
        get() {
            return navHostFragment.childFragmentManager.fragments.firstOrNull()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(findViewById(R.id.toolbar))

        checkErrors()

        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navHostFragment.childFragmentManager.addOnBackStackChangedListener(this)

        navController = navHostFragment.findNavController()
        setupActionBarWithNavController(navController)

        binding.fakeSplash.animate().setDuration(500).alpha(0f)

        collectFabState()
        collectSnackBarState()
    }

    private fun checkErrors() {
        intent.extras?.let {

            AlertDialog.Builder(this)
                .setTitle(getString(R.string.sorry))
                .setMessage(getString(R.string.detected_crash_error))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.ok)) { dialog, _ -> dialog.cancel() }
                .create().show()

            //  Report the crash to tools like Firebase or similar implementing a method like below
            //  sendCrash(it.getString(getString(R.string.message), null),
            //    it.getString(getString(R.string.cause), null))
        }
    }

    private fun collectSnackBarState() {
        lifecycleScope.launch {
            viewModel.snackBarState.collect {
                when (it) {
                    is MainViewModel.SnackBarState.Visible -> {
                        Snackbar.make(binding.coordinatorLayout, it.message, Snackbar.LENGTH_SHORT)
                            .show()
                    }
                    is MainViewModel.SnackBarState.Nothing -> Unit
                }
            }
        }
    }

    private fun collectFabState() {
        lifecycleScope.launch {
            viewModel.fabState.collect {
                when (it) {
                    is MainViewModel.FabState.Extended -> {
                        binding.fab.extend()
                    }
                    is MainViewModel.FabState.Shrunk -> {
                        binding.fab.shrink()
                    }
                    else -> Unit
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        toolbarSearchView = menu.findItem(R.id.action_search).actionView as SearchView?
        toolbarSearchView?.setOnQueryTextListener(this)
        return true
    }

    private fun setSearchViewVisibility(visible: Boolean) {
        toolbarSearchView?.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun onBackStackChanged() {
        setSearchViewVisibility(currentFragment is ISearchFragment)
        customiseFabButton()
    }

    private fun customiseFabButton() {
        val isFabFragment = currentFragment is IFabFragment
        setFabVisibility(isFabFragment)
        if (!isFabFragment) {
            return
        }
        val fragment = currentFragment as IFabFragment
        binding.fab.apply {
            setOnClickListener { fragment.onFabClick() }
            text = fragment.fabLabel
            icon = fragment.fabIcon
            if (text.isEmpty()) {
                shrink()
                return
            }
            extend()
        }
    }

    private fun setFabVisibility(visible: Boolean) {
        binding.fab.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        (currentFragment as? ISearchFragment)?.onQueryTextSubmit(query)
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        (currentFragment as? ISearchFragment)?.onQueryTextChange(newText)
        return false
    }
}