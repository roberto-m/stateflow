package com.robertomartucci.stateflow.main

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class MainViewModel @Inject constructor(): ViewModel() {

    private val _fabState = MutableStateFlow<FabState>(FabState.Nothing)
    var fabState: StateFlow<FabState> = _fabState

    private val _snackBarState = MutableStateFlow<SnackBarState>(SnackBarState.Nothing)
    var snackBarState: StateFlow<SnackBarState> = _snackBarState

    sealed class FabState {
        object Extended: FabState()
        object Shrunk: FabState()
        object Nothing: FabState()
    }

    sealed class SnackBarState {
        data class Visible(val message: String): SnackBarState()
        object Nothing: SnackBarState()
    }

    fun extend() {
        _fabState.value = FabState.Extended
    }

    fun shrink() {
        _fabState.value = FabState.Shrunk
    }

    fun showSnackBar(message: String) {
        _snackBarState.value = SnackBarState.Visible(message)
        _snackBarState.value = SnackBarState.Nothing
    }
}