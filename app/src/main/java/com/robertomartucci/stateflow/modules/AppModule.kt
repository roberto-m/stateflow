package com.robertomartucci.stateflow.modules

import android.content.Context
import com.robertomartucci.stateflow.BuildConfig
import com.robertomartucci.stateflow.repositories.products.IProductsApi
import com.robertomartucci.stateflow.repositories.products.IProductsRepository
import com.robertomartucci.stateflow.repositories.products.ProductsRepository
import com.robertomartucci.stateflow.repositories.reviews.IReviewsApi
import com.robertomartucci.stateflow.repositories.reviews.IReviewsRepository
import com.robertomartucci.stateflow.repositories.reviews.ReviewsRepository
import com.robertomartucci.stateflow.util.DispatcherProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideLocalhostIPAddress(): String {
        return BuildConfig.LOCALHOST
    }

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(if(BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        return httpClient.build()
    }

    @Singleton
    @Provides
    fun provideProductsApi(okHttpClient: OkHttpClient, localhost: String): IProductsApi = Retrofit.Builder()
        .baseUrl("http://$localhost:3001/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
        .create(IProductsApi::class.java)

    @Singleton
    @Provides
    fun provideProductsRepository(api: IProductsApi, @ApplicationContext context: Context): IProductsRepository = ProductsRepository(api, context)

    @Singleton
    @Provides
    fun provideReviewsApi(okHttpClient: OkHttpClient, localhost: String): IReviewsApi = Retrofit.Builder()
            .baseUrl("http://$localhost:3002/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(IReviewsApi::class.java)

    @Singleton
    @Provides
    fun provideReviewsRepository(api: IReviewsApi, @ApplicationContext context: Context): IReviewsRepository = ReviewsRepository(api, context)

    @Singleton
    @Provides
    fun provideDispatchers(): DispatcherProvider = object : DispatcherProvider {
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main
        override val io: CoroutineDispatcher
            get() = Dispatchers.IO
        override val default: CoroutineDispatcher
            get() = Dispatchers.Default
        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined
    }
}