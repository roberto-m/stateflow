package com.robertomartucci.stateflow.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T, VH : BaseVieHolder<T>>(private val listener: OnItemClickListener<T>? = null) :
    RecyclerView.Adapter<VH>() {

    interface OnItemClickListener<T> {

        fun onItemClick(item: T?, itemView: View)
    }

    abstract val layoutId: Int

    var items: MutableList<T> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view: View = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return onCreateHolder(view, viewType, listener)
    }

    abstract fun onCreateHolder(view: View, viewType: Int, listener: OnItemClickListener<T>?): VH

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = if(items.size > position) items[position] else null
        setOnClickListener(holder, item)
        holder.updateUI(item)
    }

    open fun setOnClickListener(holder: VH, item: T?) {
        holder.itemView.setOnClickListener { listener?.onItemClick(item, holder.itemView) }
    }

    override fun getItemCount(): Int {
        return items.count()
    }
}