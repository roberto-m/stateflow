package com.robertomartucci.stateflow.base

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseVieHolder<T>(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    val context: Context
        get() = itemView.context

    abstract fun updateUI(item: T?)
}