package com.robertomartucci.stateflow.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(
    val currency: String,
    val description: String,
    val id: String,
    val imgUrl: String,
    val name: String,
    val price: Int,
    val reviews: List<Review>
): Parcelable