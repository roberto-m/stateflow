package com.robertomartucci.stateflow.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Review(
    val locale: String,
    val productId: String,
    val rating: Int,
    val text: String
): Parcelable