package com.robertomartucci.stateflow

import android.app.Application
import android.content.Intent
import android.os.Bundle
import com.robertomartucci.stateflow.main.MainActivity
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.system.exitProcess


@ExperimentalCoroutinesApi
@HiltAndroidApp
class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Thread.setDefaultUncaughtExceptionHandler { _, e ->
            handleUncaughtException(e)
        }
    }

    private fun handleUncaughtException(e: Throwable) {
        val bundle = Bundle().apply {
            putString(getString(R.string.message), e.message)
            putString(getString(R.string.cause), e.cause.toString())
        }
        val intent = Intent(this, MainActivity::class.java).apply {
            putExtras(bundle)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        startActivity(intent)
        exitProcess(0)
    }
}