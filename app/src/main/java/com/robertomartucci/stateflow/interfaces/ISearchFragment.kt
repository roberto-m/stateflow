package com.robertomartucci.stateflow.interfaces

interface ISearchFragment {

    fun onQueryTextSubmit(query: String?)

    fun onQueryTextChange(newText: String?)
}