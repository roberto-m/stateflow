package com.robertomartucci.stateflow.interfaces

import android.graphics.drawable.Drawable

interface IFabFragment {

    val fabLabel: String?

    val fabIcon: Drawable?

    fun onFabClick()
}