package com.robertomartucci.stateflow

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.ViewModel
import com.robertomartucci.stateflow.util.DispatcherProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


@ExperimentalCoroutinesApi
abstract class TestBaseViewModel<VM : ViewModel> {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    lateinit var dispatcher: DispatcherProvider

    lateinit var viewModel: VM

    @Before
    open fun setup() {
        MockitoAnnotations.openMocks(this)
        Mockito.`when`(dispatcher.io).thenReturn(testCoroutineRule.testCoroutineDispatcher)
        configureViewModel()
    }

    abstract fun configureViewModel()
}