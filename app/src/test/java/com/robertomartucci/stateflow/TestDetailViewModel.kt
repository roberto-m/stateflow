package com.robertomartucci.stateflow

import com.robertomartucci.stateflow.detail.DetailViewModel
import com.robertomartucci.stateflow.models.Product
import com.robertomartucci.stateflow.models.Review
import com.robertomartucci.stateflow.repositories.resources.Resource
import com.robertomartucci.stateflow.repositories.reviews.IReviewsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TestDetailViewModel: TestBaseViewModel<DetailViewModel>() {

    @Mock
    lateinit var reviewsRepository: IReviewsRepository

    @Mock
    lateinit var review: Review

    @Mock
    lateinit var product: Product

    private var productId: String = "productId"
    private val text = "new review"
    private val rating = 5

    override fun configureViewModel() {
        viewModel = DetailViewModel(reviewsRepository, dispatcher)
        Mockito.`when`(product.id).thenReturn(productId)
    }

    @Test
    fun testSuccessfulPostReview(): Unit = testCoroutineRule.runBlockingTest {
        Mockito.`when`(reviewsRepository.postReview(productId, text, rating)).thenReturn(Resource.Success(review))

        viewModel.product = product
        viewModel.onNewReview(text, rating)

        assert(viewModel.detailEvent.value.equals(DetailViewModel.DetailEvent.Success(review)))
    }


    @Test
    fun testPostReviewError(): Unit = testCoroutineRule.runBlockingTest {
        val error = "error"
        Mockito.`when`(reviewsRepository.postReview(productId, text, rating)).thenReturn(Resource.Error(error))

        viewModel.product = product
        viewModel.onNewReview(text, rating)

        assert(viewModel.detailEvent.value.equals(DetailViewModel.DetailEvent.Failure(error)))
    }
}