package com.robertomartucci.stateflow

import com.robertomartucci.stateflow.models.Product
import com.robertomartucci.stateflow.products.ProductsViewModel
import com.robertomartucci.stateflow.repositories.products.IProductsRepository
import com.robertomartucci.stateflow.repositories.resources.ResourceList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TestProductsViewModel: TestBaseViewModel<ProductsViewModel>() {

    @Mock
    lateinit var productsRepository: IProductsRepository

    @Mock
    lateinit var products: List<Product>

    override fun configureViewModel() {
        viewModel = ProductsViewModel(productsRepository, dispatcher)
    }

    @Test
    fun testEmptyGetProducts(): Unit = testCoroutineRule.runBlockingTest {
        Mockito.`when`(products.isEmpty()).thenReturn(true)
        Mockito.`when`(productsRepository.getProducts()).thenReturn(ResourceList.Success(products))

        viewModel.getProducts()

        assert(viewModel.productsEvent.value.equals(ProductsViewModel.ProductsEvent.Empty))
    }

    @Test
    fun testGetProductsSuccess(): Unit = testCoroutineRule.runBlockingTest {
        Mockito.`when`(products.isEmpty()).thenReturn(false)
        Mockito.`when`(productsRepository.getProducts()).thenReturn(ResourceList.Success(products))

        viewModel.getProducts()

        assert(viewModel.productsEvent.value.equals(ProductsViewModel.ProductsEvent.Success(products)))
    }

    @Test
    fun testGetProductsError(): Unit = testCoroutineRule.runBlockingTest {
        val error = "error"
        Mockito.`when`(productsRepository.getProducts()).thenReturn(ResourceList.Error(error))

        viewModel.getProducts()

        assert(viewModel.productsEvent.value.equals(ProductsViewModel.ProductsEvent.Failure(error)))
    }
}